﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using TesteDextra.Application.Interfaces;
using TesteDextra.Application.ViewModel.Request;
using TesteDextra.Application.ViewModel.Response;
using TesteDextra.Domain.Entities;
using TesteDextra.Domain.Interfaces.Services;

namespace TesteDextra.Application.Services
{
    public class PedidoApplication : IPedidosApplication
    {
        private readonly IPedidosDomain _iPedidosService;

        public PedidoApplication(IPedidosDomain iPedidosService)
        {
            _iPedidosService = iPedidosService;
        }

        public IEnumerable<PedidosViewModel> GetAllPedidos()
        {
            return Mapper.Map<IEnumerable<Pedido>, IEnumerable<PedidosViewModel>>(_iPedidosService.GetAllPedidos());
        }

        public bool SavePedido(PedidoViewModel pedido)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _iPedidosService.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
