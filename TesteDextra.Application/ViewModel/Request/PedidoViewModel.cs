﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteDextra.Application.ViewModel.Request
{
    public class PedidoViewModel
    {
        public long IdPedido { get; set; }
        public List<Complemento> Complementos { get; set; }
    }

    public class Complemento
    {
        public long IdComplemento { get; set; }
        public int Quantidade { get; set; }
        public decimal Valor { get; set; }
    }
}
