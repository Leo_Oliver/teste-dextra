﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using TesteDextra.Application.ViewModel.Response;
using TesteDextra.Domain.Entities;

namespace TesteDextra.Application.Automapper.DomainToViewModel
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            this.Configure();
        }

        protected void Configure()
        {
            CreateMap<Pedido, PedidosViewModel>().ForMember(dest => dest.Codigo, opt => opt.MapFrom(src => src.NumeroPedido))
                                                 .ForMember(dest => dest.Complementos, opt => opt.MapFrom(src => src.PedidoIngredientes.Select(x => x.Ingrediente.Nome)))
                                                 .ForMember(dest => dest.DataHora, opt => opt.MapFrom(src => src.DataPedido))
                                                 .ForMember(dest => dest.Lanche, opt => opt.MapFrom(src => src.PedidoIngredientes.First().Ingrediente.LancheIngredientes.First().Lanche.Nome))
                                                 .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.StatusPedido.Descricao))
                                                 .ForMember(dest => dest.Valor, opt => opt.MapFrom(src => src.ValorFinal));
        }
    }
}
