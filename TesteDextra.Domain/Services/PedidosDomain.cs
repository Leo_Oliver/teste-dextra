﻿using System;
using System.Collections.Generic;
using System.Text;
using TesteDextra.Domain.Entities;
using TesteDextra.Domain.Interfaces.Repository;
using TesteDextra.Domain.Interfaces.Services;

namespace TesteDextra.Domain.Services
{
    public class PedidosDomain : IPedidosDomain, IPedidoRepository
    {
        private readonly IPedidoRepository _pedidoRepository;

        public PedidosDomain(IPedidoRepository pedidoRepository)
        {
            _pedidoRepository = pedidoRepository;
        }

        public IEnumerable<Pedido> GetAllPedidos()
        {
           return _pedidoRepository.GetAllPedidos();
        }

        public bool SavePedido(PedidoIngrediente pedido)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _pedidoRepository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
