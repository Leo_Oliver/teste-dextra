﻿using System;
using System.Collections.Generic;
using System.Text;
using TesteDextra.Domain.Entities;

namespace TesteDextra.Domain.Interfaces.Services
{
    public interface IPedidosDomain : IDisposable
    {
        IEnumerable<Pedido> GetAllPedidos();

        bool SavePedido(PedidoIngrediente pedido);

    }
}
